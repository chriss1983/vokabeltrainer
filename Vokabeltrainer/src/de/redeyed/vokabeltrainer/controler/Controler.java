package de.redeyed.vokabeltrainer.controler;

import javax.swing.JPanel;

import de.redeyed.vokabeltrainer.model.Model;
import de.redeyed.vokabeltrainer.view.BuchstabenMode;
import de.redeyed.vokabeltrainer.view.KarteikartenMode;
import de.redeyed.vokabeltrainer.view.MainView;
import de.redeyed.vokabeltrainer.view.MultibleChoiceMode;

public class Controler {
	private MainView view;
	private Model model;
	private VokabelReader vokabelReader;
	
	public enum Mode { 
		KARTEIKARTEN,
		MULTIBLECHOICE,
		BUCHSTABENSALAT
	}
	
	public Controler() {
		view = new MainView();
		model = new Model(this);
		vokabelReader = new VokabelReader();
	}
	
	
	public JPanel getMode(int modeValue) {
		Mode mode = Mode.values()[modeValue]; 
		
		switch(mode) {
			case KARTEIKARTEN: 
				return new KarteikartenMode();

			case MULTIBLECHOICE:
				return new BuchstabenMode();

			case BUCHSTABENSALAT:
				return new MultibleChoiceMode();

			default:
				return null;
		}
	}
}


