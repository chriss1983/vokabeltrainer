package de.redeyed.vokabeltrainer.model;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Properties;

public class User {
	private static final long serialVersionUID = 1L;
	
	private Profil profil;
	private String name;
	private int erreichtePunkte;
	Properties defaultProperties = new Properties(),
	           userProperties    = new Properties( defaultProperties );
	
	
	public User(String name) {
		this.name = name;
		prop();
	}
	
	public void prop() {
		defaultProperties.setProperty( "User", "C.Ullenboom" );
		defaultProperties.setProperty( "Password", "(nicht gesetzt)" );
		userProperties.setProperty( "Password", "SagIchNet" );
		
		FileOutputStream writer = null;
		FileOutputStream reader = null;

		try
		{
		  writer = new FileOutputStream("properties.txt");
		  
		  Properties prop1 = new Properties( System.getProperties() );
		//  defaultProperties.setProperty( "MeinNameIst", "Forrest Gump" );
		  defaultProperties.storeToXML(writer, "TEst"); 
		  userProperties.storeToXML(writer, "TEst"); 
	//	  reader = new FileOutputStream( "properties.txt" );

		/*  Properties prop2 = new Properties();
		  prop2.load( reader );
		  prop2.list( System.out );
		  */
		}
		catch ( IOException e )
		{
		  e.printStackTrace();
		}
		finally
		{
		  try { writer.close(); } catch ( Exception e ) { }
		  try { reader.close(); } catch ( Exception e ) { }
		}
	}
}
