package de.redeyed.vokabeltrainer.model;

import java.io.Serializable;

public class Profil implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private double erreichtePunkte;
	private double prozentRichtig;
	private double prozentFalsch;
	private double nichtBeantwortet;
	
	public double getErreichtePunkte() {
		return erreichtePunkte;
	}
	public void setErreichtePunkte(double erreichtePunkte) {
		this.erreichtePunkte = erreichtePunkte;
	}
	public double getProzentRichtig() {
		return prozentRichtig;
	}
	public void setProzentRichtig(double prozentRichtig) {
		this.prozentRichtig = prozentRichtig;
	}
	public double getProzentFalsch() {
		return prozentFalsch;
	}
	public void setProzentFalsch(double prozentFalsch) {
		this.prozentFalsch = prozentFalsch;
	}
	public double getNichtBeantwortet() {
		return nichtBeantwortet;
	}
	public void setNichtBeantwortet(double nichtBeantwortet) {
		this.nichtBeantwortet = nichtBeantwortet;
	}
	
	
	/*
	-	Was war richtig?
			-	Was war falsch?
			-	Wieviel % richtig/falsch
			-	Wieviele nicht beantwortet(auch in %)
	*/
}
